# OBUtils
This repository contains a collection of small helper scripts that I wrote to accompany and complete my Openbox session.
The following scripts are contained herein:
- `obdevctl`: uses `nmcli` to toggle the state of the WLAN antenna, display, touchpad, and mouse.  *Adjust the names to suit your devices (`xinput list`)*;
- `obscrot`: leverages `scrot` to take screenshots. *No support for other screenshooters is (yet) implemented*;
- `obsetbg`: shows a menu showing all images in a given background directory. Utilizes `nitrogen` to set the background.  *No other setters are (yet) supported*.
- `obsetstyle`: shows a menu showing all installed Openbox styles, and allows switching on-the-fly between them. *As of now, this only supports system-wide styles, no user-specific ones.*
- `obplaces`: shows a menu including all non-hidden directories within $HOME.  A file manager can be specified within the script by changing the `fm` variable.

New scripts may be added in the future.

## Dependencies
- `obdevctl`: `networkmanager` to manage networks, `xorg-xinput` to manage input devices, 'xorg-xset' to manage the display;
- `obscrot`: `scrot` to take the screenshot;
- `obsetbg`: `nitrogen` to set the background, and open the background picker dialog.
- `obsetstyle`: `find`, `sed` and `awk`
- `obplaces`: `find` and `sort`
